import {resources} from 'Managers/ResourceManager';
import {log} from 'Log';
import {InputMan} from 'Managers/InputManager';
import {Scripts} from 'Scripts/Scripts';
import {EventMan} from 'Managers/EventManager';
import cfg from 'config.json';

class Entity extends PIXI.Container {
  /* TODO: How (and when) initialize scripts? Need planning.
  It cant be done at construction time, as script might need to find other entities.
  How to control sctipt init and update order.
  Could `components` be entirely scraped? Like unity, component data could be in scripts.
  Would this be useful/easy/more convenient?
  */

  constructor(data) {
    super();
    this.eventTypes = [];
    this.events = [];
    this.isActive = true;
    this.tags = [];
    this.scripts = [];
  }

  init(rootEntity){
    this.children.forEach((child) => {
      if(child.init) child.init(rootEntity);
    });
    this.scripts.forEach((script) => {
      script.init(this, rootEntity);
    });
    EventMan.registerListener(this);
  }

  // TODO: Check if event is relevant to the script.
  handleEvents() {
    this.events.forEach((evt) => {
      this.scripts.forEach((script) => {
        script.handleGameEvent(this, evt);
      });
    });
    this.events = [];
  }

  findEntityWithTag(tag){
    let indof = this.tags.indexOf(tag);
    if(indof >= 0){ return this; }
    else {
      for(let i = 0; i < this.children.length;i++){
        let child = this.children[i];
        let found;
        if(child.findEntityWithTag){
          found = child.findEntityWithTag(tag);
        }
        if(found){
          return found;
        }
      }
    }
  }

  findEntityWithName(name){
    if(this.name === name){ return this; }
    else {
      for(let i = 0; i < this.children.length;i++){
        let child = this.children[i];
        let found;
        if(child.findEntityWithName){
          found = child.findEntityWithName(name);
        }
        if(found){
          return found;
        }
      }
    }
  }

  constructPixiPolygon(vertices, offset) {
    let l = [];
    vertices.forEach((vertex) => {
      l.push(vertex.x - offset.x);
      l.push(vertex.y - offset.y);
    });
    return l;
  }

  createBox2DVertices(vertices, offset) {
    let verts = [];
    vertices.forEach((v) => {
      let vec = new b2Vec2(
        v.x * 1.0 / cfg.physicsScale,
        v.y * 1.0 / cfg.physicsScale
      );
      verts.push(vec);
      // console.log(vec.get_x() + " " + vec.get_y());
    });
    return verts;
  }

  addDebugGraphics() {
    let physics = this.physics;
    if (physics) {
      let body = physics.body;
      let width = physics.body.bounds.max.x - physics.body.bounds.min.x;
      let height = physics.body.bounds.min.y - physics.body.bounds.max.y;
      this.debugGraphics = new PIXI.Graphics();
      let color = this.collider_color || body.render.fillStyle;

      // log.debug(body);
      this.debugGraphics.beginFill(color);
      this.debugGraphics.lineStyle(2, body.render.strokeStyle);
      this.debugGraphics.alpha = 0.5;

      let poly = this.constructPixiPolygon(body.vertices, body.position);

      // this.debugGraphics.drawRect(0, 0, width, height);
      this.debugGraphics.drawPolygon(poly);
      // this.addChild(this.debugGraphics);
    }

  }


  // TODO: Remove duplicate event types (keep only topmost)
  addScript(scriptName, parameters) {
    let script = new Scripts[scriptName](parameters);
    this.scripts.push(script);
    let eventTypes = script.eventTypes;
    eventTypes.forEach((eventType) => {
      this.eventTypes.push(eventType);
    });
  }

  addEvent(evt) {
    this.events.push(evt);
  }

  addBox(color, width, height) {
    let graphics = new PIXI.Graphics();
    graphics.beginFill(color);
    graphics.drawRect(0, 0, width, height);
    graphics.pivot = {
      x: width/2,
      y: height/2
    };

    this.addChild(graphics);
  }

  addCircle(color, radius) {
    let graphics = new PIXI.Graphics();
    graphics.beginFill(color);
    graphics.drawCircle(0, 0, radius);

    this.addChild(graphics);
  }

  addPolygon(color, vertices) {
    let graphics = new PIXI.Graphics();
    graphics.beginFill(color);
    graphics.drawPolygon(this.constructPixiPolygon(vertices, {x: 0, y: 0}));

    this.addChild(graphics);
  }

  addShape(shape) {

  }

  setSprite(spriteName){
    if (!this.sprite) {
      this.sprite = new PIXI.Sprite();
      this.sprite.anchor = {
        x: 0.5,
        y: 0.5
      };
      this.addChild(this.sprite);
    }
    this.sprite.texture = resources.sprite.textures[spriteName];
  }

  // https://github.com/wellcaffeinated/PhysicsJS/wiki/Fundamentals#the-factory-pattern
  addPhysics(bodyType, options = {}) {
    let bodyDef = new b2BodyDef();
    let fixDef = new b2FixtureDef();
    let width = options.width;
    let height = options.height;
    let radius = width / 2.0;
    let x = options.x;
    let y = options.y;
    let pos = new Box2D.b2Vec2(
      options.x / cfg.physicsScale ,
      options.y / cfg.physicsScale
    );
    let shape = {};

    switch (bodyType) {
      case 'circle':
        shape = new b2CircleShape(radius);
        shape.set_m_radius(radius / cfg.physicsScale);
        if (options.render) {
          this.addCircle(this.collider_color, width/2);
        }
        break;
      case 'rectangle':
        shape = new b2PolygonShape();
        shape.SetAsBox(
          width / cfg.physicsScale / 2,
          height / cfg.physicsScale / 2
        );
        if (options.render) {
          this.addBox(this.collider_color, width, height);
        }
        break;
      case 'polygon':
        let verts = this.createBox2DVertices(options.polygon);
        shape = createPolygonShape( verts );
        if (options.render) {
          this.addPolygon(this.collider_color, options.polygon);
        }
        break;
    }
    if (options.treatment !== 'static') {
      bodyDef.set_type(Box2D.b2_dynamicBody);
      bodyDef.set_linearDamping(options.damping || 0.0);
      console.log(bodyDef);
    }
    bodyDef.set_position(pos);

    fixDef.set_density(options.density || 1.0);
    fixDef.set_friction(options.friction || 0.5);
    fixDef.set_restitution(options.restitution || 0.2);
    fixDef.set_shape(shape);


    this.physics = {
      inWorld: false,
      bodyDef: bodyDef,
      fixDef: fixDef
    };
  }

  /*
    Unpacks entity from configuration file. Loads config
    Config format:
      - component_data
        - Will go straight to entity.
          Useful when defining components that dont need their own config files
      - component_configuration
        - Holds a handle for config file that holds the actual data.
          Useful when actual component data is in another file. Like animations.
    Create entity with this and see its structure for more info.
  */
  static fromConfig(configName) {
    return Entity.fromConfigObj(resources[configName].data);
  }

  static fromConfigObj(config) {
    const ent = new Entity();

    // Assign component_data to entity
    Object.assign(ent, config.component_data);

    // Get each component_configuration and set them to entity
    const compConf = config.component_configuration;
    Object.keys(compConf).forEach(key => {
      ent[key] = resources[compConf[key]].data;
    });

    const physics = config.physics;
    if (physics) {
      ent.addPhysics(physics.bodyType, physics.options);
      // if (cfg.debugMode) ent.addDebugGraphics();
    }

    if (config.sprite) {
      ent.setSprite(config.sprite);
    }

    const scriptConf = config.scripts;
    scriptConf.forEach(conf => {
      const name = conf.name;
      const params = conf.parameters || {};
      ent.addScript(name, params);
    });
    return ent;
  }

  static fromTiledObject(tiledObj) {
    let props = tiledObj.properties;
    let config = resources[props.config].data;

    Object.assign(config.component_data, tiledObj.properties);

    config.component_data.name = tiledObj.name;

    config.physics.options.x = tiledObj.x + tiledObj.width/2;
    config.physics.options.y = tiledObj.y + tiledObj.height/2;
    config.physics.options.width = tiledObj.width;
    config.physics.options.height = tiledObj.height;
    config.physics.options.radius = tiledObj.width/2;
    config.physics.options.restitution = tiledObj.res;
    if (tiledObj.polygon) {
      config.physics.options.polygon = tiledObj.polygon;
    }

    let ent = Entity.fromConfigObj(config);
    // log.debug(ent);
    return ent;
  }
}

export {Entity};
