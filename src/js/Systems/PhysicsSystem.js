import {System} from 'System';
import {log} from 'Log';
import cfg from 'config.json';
import {EventMan} from 'Managers/EventManager';
import {PhysicsUtil} from 'Util/PhysicsUtil';

class PhysicsSystem extends System {
  constructor(timeStep = 3, maxIPF = 16, integrator = 'verlet') {
    super();
    this.box2DShortcuts();
    const gravity = new b2Vec2(0, 0.0);
    const allowSleep = false;
    this.world = new b2World(gravity);

    // var bd_ground = new Box2D.b2BodyDef();
    // var ground = world.CreateBody(bd_ground);
    // var shape0 = new Box2D.b2EdgeShape();
    // shape0.Set(new Box2D.b2Vec2(-40.0, -6.0), new Box2D.b2Vec2(40.0, -6.0));
    // ground.CreateFixture(shape0, 0.0);


    this.timer = 0;
    this.physicsScale = 16;
    // this.engine = Matter.Engine.create();
    // this.engine.timing.timeScale = 0.5;
    // this.world = this.engine.world;
    // this.world.gravity = {x: 0, y: 0};
    this.events = [];
    this.eventTypes = ['physics'];
    if (cfg.debugMode) this.debug();
  }

  box2DShortcuts() {
    window.b2Vec2 = Box2D.b2Vec2;
    window.b2AABB = Box2D.b2AABB;
    window.b2BodyDef = Box2D.b2BodyDef;
    window.b2Body = Box2D.b2Body;
    window.b2FixtureDef = Box2D.b2FixtureDef;
    window.b2Fixture = Box2D.b2Fixture;
    window.b2World = Box2D.b2World;
    window.b2MassData = Box2D.b2MassData;
    window.b2PolygonShape = Box2D.b2PolygonShape;
    window.b2CircleShape = Box2D.b2CircleShape;
    window.b2DebugDraw = Box2D.b2DebugDraw;
    window.b2MouseJointDef =  Box2D.b2MouseJointDef;
  }

  init() {
    EventMan.registerListener(this);
  }

  debug() {

  }

  handleEvents() {
    // this.events.forEach((evt) => {
    //   this.scripts.forEach((script) => {
    //     script.handleGameEvent(this, evt);
    //   });
    // });
    this.events = [];
  }

  applySystem(entity, rootEntity, delta) {
    if (entity.physics) {
      // Add the entity if it isn't in the world yet
      if (!entity.physics.inWorld) {
        entity.physics.body = this.world.CreateBody(entity.physics.bodyDef);
        let bod = entity.physics.body;
        entity.physics.fixture = bod.CreateFixture(entity.physics.fixDef);

        bod.SetAwake(1);
        bod.SetActive(1);
        // Matter.World.add(this.world, entity.physics.body);
        entity.physics.inWorld = true;
        // console.log(entity.physics.fixture);
        // console.log(entity.physics.body.GetPosition().get_x());
      }
      // Update the position of the entity to that of the
      // body

      let data = PhysicsUtil.objectData(entity.physics.body);
      entity.position = {
        x: data.x,
        y: data.y
      };

      entity.rotation = data.angle;
    }
  }


  updateSystem(rootEntity, delta) {
    this.timer += delta;
    this.world.Step(delta/1000.0, 2, 2);
  }
}

export {PhysicsSystem};
