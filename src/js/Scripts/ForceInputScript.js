import {log} from 'Log';
import {Script} from 'Script';
import {InputMan as Input} from 'Managers/InputManager';
import {EventMan} from 'Managers/EventManager';
import {resources} from 'Managers/ResourceManager';
import {Entity} from 'Entity';
import cfg from 'config.json';

class ForceInputScript extends Script {
  constructor(parameters) {
    super(parameters);
    this.eventTypes.push(
      'input_test'
    );
    this.inputting = false;
  }

  init(parent, rootEntity) {
    this.arrowSprite = new PIXI.Sprite();
    this.arrowSprite.pivot = {x: 512, y: 128};
    this.arrowSprite.anchor = {
      x: 0,
      y: 0
    };
    this.arrowSprite.texture = resources.sprite.textures['arrow'];
    parent.addChild(this.arrowSprite);
    this.arrowSprite.scale = {x: 1.0, y: 0.1};
    // this.arrowEntity = Entity.fromConfig('entity_force_arrow');
    // log.debug(this.arrowEntity);
    // parent.children.push(this.arrowEntity);
    // parent.setSprite('debug_2');
  }

  testCollision(parent, point) {
    if (parent.physics) {
      let bod = parent.physics.body;
      let fixtures = bod.GetFixtureList();
      let fix = parent.physics.fixture;
      // console.log(fixtures);
      // while (i < fixtures.length) {
      //   i++;
      // }
      // console.log(bod);
      let p = new b2Vec2(point.x/cfg.physicsScale, point.y/cfg.physicsScale);
      let pos = bod.GetPosition();
      console.log("x: " + p.get_x() + " y: " + p.get_y());
      console.log("x: " + pos.get_x() + " y: " + pos.get_y());
      let t = fixtures.TestPoint(p);
      if (t) console.log(t);
      return t;
      //  return (fix.TestPoint(p));
      // return Matter.Bounds.contains(bod.bounds, point) && Matter.Vertices.contains(bod.vertices, point);
    } else {
      // TODO
      return false;
    }
  }

  length(x, y) {
    return Math.sqrt(x*x + y*y);
  }

  update(parent, rootEntity, delta) {
    let pos = Input.mousePos;
    if (parent.physics) {
      let bod = parent.physics.body;
      if (Input.mousePressed && this.testCollision(parent, pos)) {
        this.inputting = true;
      } else if (Input.mouseReleased && this.inputting) {
        let bodPos = bod.GetPosition();
        let bPos = {
          x: bodPos.get_x() * cfg.physicsScale,
          y: bodPos.get_y() * cfg.physicsScale
        };
        this.inputting = false;
        let difX = bPos.x - pos.x;
        let difY = bPos.y - pos.y;
        console.log("APPLY FORCE");
        bod.ApplyLinearImpulse(new b2Vec2(difX, difY), bod.GetWorldCenter(), true);
        console.log(bod.GetLinearVelocity().get_y());
        // Matter.Body.applyForce(bod, bodPos, {x: difX / 500.0, y: difY / 500.0});
      }
      if (this.inputting) {
        let bodPos = bod.GetPosition();
        let bPos = {
          x: bodPos.get_x() * cfg.physicsScale,
          y: bodPos.get_y() * cfg.physicsScale
        };
        this.arrowSprite.alpha = 0.8;
        let difX = bPos.x - pos.x;
        let difY = bPos.y - pos.y;

        const l = this.length(difX, difY);

        // console.log(l);

        const ang = Math.atan2(difY / l, difX / l);

        console.log(ang);
        this.arrowSprite.rotation = ang - parent.rotation;
        this.arrowSprite.scale.x = l/512.0;
      } else {
        this.arrowSprite.alpha = 0.0;
      }
    }
  }

  handleGameEvent(parent, evt) {
    log.debug(evt.parameters.message);
  }
}

export {ForceInputScript};
