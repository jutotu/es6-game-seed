import {InputScript} from './InputScript';
import {AnimationScript} from './AnimationScript';
import {ForceInputScript} from './ForceInputScript';

const scripts = {
  inputScript: InputScript,
  forceInputScript: ForceInputScript,
  animationScript: AnimationScript
};

export {scripts as Scripts};
