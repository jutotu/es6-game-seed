import {log} from 'Log';

class Script {
  constructor(parameters) {
    let a = {};
    jQuery.extend(true, a, parameters);
    Object.assign(this, a);

    this.eventTypes = [];
  }

  // TODO: Script initialization.
  init(parent, rootEntity) {}

  update(parameters, parent, rootEntity, delta) {}
  lateUpdate(parameters, parent, rootEntity, delta) {}

  handleGameEvent(parent, evt) {}
}

export {Script};
