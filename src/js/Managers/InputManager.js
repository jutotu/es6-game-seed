import keycfg from 'keys.json';
import {Manager} from 'Manager';
import cfg from 'config.json';
import {log} from 'Log';

const $ = jQuery;

class InputManager extends Manager {
  constructor(){
    super();
    this.keyDown = {};
    this.keyPressed = {};
    this.keyReleased = {};
    this.mousePos = {x: 0, y: 0};
    this.mousePressed = false;
    this.mouseReleased = false;
  }

  init(){
    const promise = new Promise((resolve, reject) => {

      this.keys = new Map( Object.keys(keycfg).map(key => {
          return [keycfg[key], key];
        }));

      window.addEventListener('keydown', (e) => this.setKeyState(e, true), false);
      window.addEventListener('keyup', (e) => this.setKeyState(e, false), false);
      jQuery(window).mousedown((e) => this.setMouseState(e, true));
      jQuery(window).mouseup((e) => this.setMouseState(e, false));
      jQuery(window).bind('mousemove', (e) => this.setCursorPos(e));

      resolve('Input manager init done!');

    });
    this.el = $(cfg.container);

    return promise;
  }

  setKeyState(ev, state) {
    const code = ev.which;
    const key = this.keys.get(code);
    if (key) ev.preventDefault();
    if (this.keyDown[key] != state) {
      this.keyDown[key] = state;
      if (state) {
        this.keyPressed[key] = true;
      } else {
        this.keyReleased[key] = true;
      }
    }
  }

  setMouseState(ev, state) {
    if (this.mouseDown != state) {
      this.mouseDown = state;
    }
    if (state) {
      this.mousePressed = true;
    } else {
      this.mouseReleased = true;
    }
  }

  setCursorPos(ev) {
    const off = this.el.offset();
    this.mousePos = {x: ev.pageX - off.left, y: ev.pageY - off.top};
  }

  update(){
    /*TODO: Ensure input stays constant throughout game update.
    Keydown and keyup events will trigger even when game is updating.*/
    this.keyPressed = {};
    this.keyReleased = {};
    this.mousePressed = false;
    this.mouseReleased = false;
  }

}

const InputMan = new InputManager();

export {InputMan};
