import {log} from 'Log';
import {ScriptSystem} from 'Systems/ScriptSystem';
import {EventSystem} from 'Systems/EventSystem';
import {PhysicsSystem} from 'Systems/PhysicsSystem';
import {Entity} from 'Entity';
import {Scripts} from 'Scripts/Scripts';
import {EventMan} from 'Managers/EventManager';
import {resources} from 'Managers/ResourceManager';
import cfg from 'config.json';

class Game {
  constructor() {
    this.stage = new Entity();
    this.world = new Entity();
    this.ui = new Entity(); //new Entity('entity_ui');

    this.stage.addChild(this.ui);
    this.stage.addChild(this.world);

    this.systems = [];
    let scriptSystem = new ScriptSystem();
    this.systems.push(scriptSystem);

    let eventSystem = new EventSystem();
    this.systems.push(eventSystem);

    let physicsSystem = new PhysicsSystem();
    this.systems.push(physicsSystem);

    this.addEntityToWorld(this.loadMap('testmap'));
    // if (cfg.debugMode) this.debugConstructor();
    this.stage.init(this.stage);
  }

  debugConstructor() {
    // this.addEntityToWorld(this.loadMap('testmap'));
    log.debug("Starting debug constructor");
    let testEntity = Entity.fromConfig('entity_player');
    log.debug("Created from config");
    testEntity.setSprite('debug_2');
    testEntity.addScript('inputScript', {a: 'b', c: 'd'});
    log.debug("Added script");
    testEntity.addPhysics('rectangle', {
      x: 0,
      y: 0,
      vx: 0.01,
      width: 64,
      height: 64
    });
    log.debug("Added physics");
    log.debug(testEntity);
    this.addEntityToWorld(testEntity);

  }

  addEntityToWorld(entity) {
    this.world.addChild(entity);
  }

  addEntityToUI(entity) {
    this.ui.addChild(entity);
  }


  update(delta) {
    this.systems.forEach((system) => {
      system.update(this.world, delta);
      system.update(this.ui, delta);
    });
  }

  render(renderer) {
    renderer.render(this.stage);
  }

  loadMap(mapname) {
    console.log(resources);
    // let a = resources[mapname].data.properties.config
    // console.log(a);
    let eMap = new Entity(); //Entity.fromConfig(a);
    log.debug(mapname);
    resources[mapname].data.layers.forEach(layer => {
      let eLayer = new Entity();
      // console.log(layer);
      if (layer.type === 'imagelayer'){
        let imagename = layer.image.split('.')[0];
        let sprite = new PIXI.Sprite();
        sprite.texture = resources[imagename].texture;
        eLayer.position.x = layer.offsetx || 0;
        eLayer.position.y = layer.offsety || 0;
        eLayer.addChild(sprite);
      }
      else if (layer.type === 'objectgroup'){
        layer.objects.forEach(obj => {
          let eObj = Entity.fromTiledObject(obj);
          eLayer.addChild(eObj);
        });
      }
      eMap.addChild(eLayer);
    });
    log.debug(eMap);
    return eMap;
  }
}

export {Game};
