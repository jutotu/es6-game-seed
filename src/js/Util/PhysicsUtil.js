import {log} from 'Log';
import cfg from 'config.json';

class PhysicsUtil {
  static objectData(body) {
    let data = {};
    const pos = body.GetPosition();
    data.x = pos.get_x() * cfg.physicsScale;
    data.y = pos.get_y() * cfg.physicsScale;
    data.angle = body.GetAngle();
    return data;
  }
}

export {PhysicsUtil};
